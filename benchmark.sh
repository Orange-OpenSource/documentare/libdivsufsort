#!/bin/sh

INPUT_DIRECTORY=$1
C_EXE=$2
JAVA_EXE=$3
C_OUTPUT_DIRECTORY=$4
JAVA_OUTPUT_DIRECTORY=$5
C_LOG=benchmark_c.log
JAVA_LOG=benchmark_java.log

rm -f $C_LOG $JAVA_LOG


$C_EXE "$INPUT_DIRECTORY" "$C_OUTPUT_DIRECTORY"  >  $C_LOG
$JAVA_EXE -i "$INPUT_DIRECTORY" -o "$JAVA_OUTPUT_DIRECTORY"  >  $JAVA_LOG

for FILE in $(ls $INPUT_DIRECTORY)
do 
    diff "$C_OUTPUT_DIRECTORY/$FILE-c.sa" "$JAVA_OUTPUT_DIRECTORY/$FILE.java-sa"
    if [ "$?" != "0" ]
    then
        echo ERROR
        exit 128
    fi
done