#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>

#include <divsufsort.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char *argv[])
{
  struct timeval start,end, diff;

  if (argc < 2)
  {
    printf("usage: sa INPUT_FILE\n");
    return -1;
  }
  

  char *filePath = argv[1];
  FILE *inputFilePtr = fopen(filePath, "rb");
  if (inputFilePtr == NULL)
  {
    printf("[ERROR] failed to open file '%s': %s\n", filePath, strerror(errno));
    return -2;
  }

  char *outputFilePath = "sa_c.out";
  FILE *outputFilePtr = fopen(outputFilePath, "w");
  if (outputFilePtr == NULL)
  {
    printf("[ERROR] failed to open file '%s': %s\n", filePath, strerror(errno));
    return -2;
  }


  fseek(inputFilePtr, 0, SEEK_END);
  long filelen = ftell(inputFilePtr);
  rewind(inputFilePtr);

  int n = filelen * 2;
  char *buffer = (char *)malloc(n * sizeof(char));
  fread(buffer, filelen, 1, inputFilePtr);
  memcpy(buffer + filelen, buffer, filelen);
  fclose(inputFilePtr);

  // intput data
  char *Text = buffer;

  // allocate
  int *SA = (int *)malloc(n * sizeof(int));

  // sort
  gettimeofday(&start, NULL);
  divsufsort((unsigned char *)Text, SA, n);
  gettimeofday(&end, NULL);
  timersub(&end, &start, &diff);
  long ms = diff.tv_usec / 1000;
  long us = diff.tv_usec - ms * 1000;
  printf("divsufsort took %ld s %ld ms %ld us for file %s\n", diff.tv_sec, ms, us, filePath);

  // output
  /*
  for (i = 0; i < n; ++i)
  {
    printf("SA[%2d] = %2d: ", i, SA[i]);
    for (j = SA[i]; j < n; ++j)
    {
      printf("%c", Text[j]);
    }
    printf("\n");
  }
  */

  for (int i = 0; i < n; ++i)
  {
    int saIndex = SA[i];
    if (saIndex < (n / 2))
    {
      fprintf(outputFilePtr, "%d ", saIndex);
    }
  }
  fprintf(outputFilePtr, "\n");
  fclose(outputFilePtr);

  // deallocate
  free(SA);
  free(buffer);
  
 
 
  
  return 0;
}
