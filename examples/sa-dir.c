#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <string.h>

#include <divsufsort.h>
#include <time.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>

struct File_Names {
  int count;
  char **filenames;
  char **filenamesAbsolutePath;
};

void checkAndClearOutputDirectory(char * outputDirectoryPath);
void retrieveInputFiles(char * inputDirectoryPath, struct File_Names * inputFileNames);
void buildOutputFileNames(struct File_Names * inputFileNames, char * outputDirectoryPath, struct File_Names * outputFileNames);
void doTheJob(char * inputFilePath, char * inputFileName, char * outputFilePath, struct timeval  * total);

int main(int argc, char *argv[])
{
  if (argc < 3)
  {
    printf("usage: sa-dir INPUT_DIRECTORY OUTPUT_DIRECTORY\n");
    return -1;
  }

  char * outputDirectoryPath = argv[2];

  checkAndClearOutputDirectory(outputDirectoryPath);

  char * inputDirectoryPath = argv[1];

  struct timeval total;
  total = (struct timeval) {0};
  struct File_Names inputFileNames;
  retrieveInputFiles(inputDirectoryPath, &inputFileNames);

  struct File_Names outputFileNames;
  buildOutputFileNames(&inputFileNames, outputDirectoryPath, &outputFileNames);

/*
  for (int i = 0; i < inputFileNames.count; i++) {
    printf("input  file %s - %s\n", inputFileNames.filenamesAbsolutePath[i], inputFileNames.filenames[i]);
    printf("output file %s - %s\n", outputFileNames.filenamesAbsolutePath[i], outputFileNames.filenames[i]);
  }
*/

  for (int i = 0; i < inputFileNames.count; i++) {
    doTheJob(inputFileNames.filenamesAbsolutePath[i], inputFileNames.filenames[i], outputFileNames.filenamesAbsolutePath[i], &total);
  }

  long s = total.tv_sec;
  long ms = total.tv_usec / 1000;
  long us = total.tv_usec - ms * 1000;
  printf("divsufsort total took %ld s %ld ms %ld us for directory %s\n", s, ms, us, inputDirectoryPath);

  return 0;
}


void doTheJob(char * inputFilePath, char* inputFileName, char * outputFilePath, struct timeval * total) {
  FILE *inputFilePtr = fopen(inputFilePath, "rb");
  if (inputFilePtr == NULL)
  {
    printf("[ERROR] failed to open file '%s': %s\n", inputFilePath, strerror(errno));
    exit(-20);
  }

  FILE *outputFilePtr = fopen(outputFilePath, "w");
  if (outputFilePtr == NULL)
  {
    printf("[ERROR] failed to open file '%s': %s\n", outputFilePath, strerror(errno));
    exit(-21);
  }


  fseek(inputFilePtr, 0, SEEK_END);
  long filelen = ftell(inputFilePtr);
  rewind(inputFilePtr);

  int n = filelen * 2;
  char *buffer = (char *)malloc(n * sizeof(char));
  fread(buffer, filelen, 1, inputFilePtr);
  memcpy(buffer + filelen, buffer, filelen);
  fclose(inputFilePtr);

  // intput data
  char *Text = buffer;

  // allocate
  int *SA = (int *)malloc(n * sizeof(int));

  // sort
  struct timeval start,end, diff;
  gettimeofday(&start, NULL);
  divsufsort((unsigned char *)Text, SA, n);
  gettimeofday(&end, NULL);
  timersub(&end, &start, &diff);
  long ms = diff.tv_usec / 1000;
  long us = diff.tv_usec - ms * 1000;
  printf("divsufsort took %ld s %ld ms %ld us for file %s\n", diff.tv_sec, ms, us, inputFileName);
  timeradd(total, &diff, total);

  for (int i = 0; i < n; ++i)
  {
    int saIndex = SA[i];
    if (saIndex < (n / 2))
    {
      fprintf(outputFilePtr, "%d ", saIndex);
    }
  }
  fprintf(outputFilePtr, "\n");
  fclose(outputFilePtr);

  // deallocate
  free(SA);
  free(buffer);
}


void checkAndClearOutputDirectory(char * outputDirectoryPath) {
  // CHECK OUTPUT DIRECTORY
  DIR *outputDirectory = opendir (outputDirectoryPath);
  if (outputDirectory == NULL) {
    printf("[ERROR] failed to open output directory '%s': %s\n", outputDirectoryPath, strerror(errno));
    exit(-2);
  }
  //

  // CLEAR OUTPUT DIRECTORY
  struct dirent *directoryFile;
  while ((directoryFile = readdir(outputDirectory))) {
      if (directoryFile->d_type != DT_REG) {
        continue;
      }
      char* filePath;
      if(0 > asprintf(&filePath, "%s/%s", outputDirectoryPath, directoryFile->d_name)) {
        printf("[ERROR] failes to format %s-%s\n", outputDirectoryPath, directoryFile->d_name);
        exit(-3);
      }
      unlink(filePath);
  }
  //

    closedir(outputDirectory);
}


void retrieveInputFiles(char * inputDirectoryPath, struct File_Names * inputFileNames) {
  DIR *inputDirectory = opendir (inputDirectoryPath);
  if (inputDirectory == NULL) {
    printf("[ERROR] failed to open input directory '%s': %s\n", inputDirectoryPath, strerror(errno));
    exit(-10);
  }

  struct dirent *directoryFile;
  int filesCount = 0;
  while ((directoryFile = readdir(inputDirectory))) {
      if (directoryFile->d_type == DT_REG) {
        filesCount++;
      }
  }
  rewinddir(inputDirectory);

  inputFileNames->count = filesCount;
  inputFileNames->filenamesAbsolutePath = malloc(filesCount * sizeof(char *));
  inputFileNames->filenames = malloc(filesCount * sizeof(char *));

  char** inputFilenamesAbsolutePath = inputFileNames->filenamesAbsolutePath;
  char** inputFilenames = inputFileNames->filenames;
  int index = 0;
  while ((directoryFile = readdir(inputDirectory))) {
      if (directoryFile->d_type == DT_REG) {
        char* fileAbsPath;
        if(0 > asprintf(&fileAbsPath, "%s/%s", inputDirectoryPath, directoryFile->d_name)) {
          printf("[ERROR] failed to format %s-%s\n", inputDirectoryPath, directoryFile->d_name);
          exit(-11);
        }
        char* fileName;
        if(0 > asprintf(&fileName, "%s", directoryFile->d_name)) {
          printf("[ERROR] failed to format %s\n", directoryFile->d_name);
          exit(-12);
        }

        inputFilenamesAbsolutePath[index] = fileAbsPath;
        inputFilenames[index] = fileName;
        index++;
      }
  }

  closedir(inputDirectory);
}


void buildOutputFileNames(struct File_Names * inputFileNames, char * outputDirectoryPath, struct File_Names * outputFileNames) {
  outputFileNames->count = inputFileNames->count;
  outputFileNames->filenamesAbsolutePath = malloc(outputFileNames->count * sizeof(char *));
  outputFileNames->filenames = malloc(outputFileNames->count * sizeof(char *));

  for (int i = 0; i < outputFileNames->count; i++) {
      char* fileAbsPath;
      if(0 > asprintf(&fileAbsPath, "%s/%s-c.sa", outputDirectoryPath, inputFileNames->filenames[i])) {
        printf("[ERROR] failed to format %s-%s\n", outputDirectoryPath, inputFileNames->filenames[i]);
        exit(-31);
      }
      char* fileName;
      if(0 > asprintf(&fileName, "%s-c.sa", inputFileNames->filenames[i])) {
        printf("[ERROR] failed to format %s\n", inputFileNames->filenames[i]);
        exit(-32);
      }
      outputFileNames->filenamesAbsolutePath[i] = fileAbsPath;
      outputFileNames->filenames[i] = fileName;
  }
}