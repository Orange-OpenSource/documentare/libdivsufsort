#!/bin/sh

if [ -z ${JNI_INCLUDE_PATH+x} ]
then
    echo "[ERROR] you must set JNI_INCLUDE_PATH environment variable, based on your jdk installation configuration"
    echo "        for instance on macos: export JNI_INCLUDE_PATH=/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/include"
    echo "                     on linux: export JNI_INCLUDE_PATH=/usr/lib/jvm/java-8-openjdk-amd64/include/"
    exit 1
fi

if [ -z "$COMPILER_COMMAND" ]
then
	COMPILER_COMMAND=gcc-10
fi

rm -rf build && \
mkdir build && \
(cd build && cmake -D CMAKE_C_COMPILER=${COMPILER_COMMAND} -D CMAKE_CXX_COMPILER=${COMPILER_COMMAND} -D JNI_INCLUDE_PATH="${JNI_INCLUDE_PATH}" .. && make) && \
echo && \
echo Please find built libraries under build/lib directory: && \
ls build/lib && \
echo && \
echo [OK]
